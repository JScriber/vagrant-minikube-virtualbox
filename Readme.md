# Usage 

Create your VM:
```
vagrant up
```

Enter in the VM:
```
vagrant ssh
```

Delete the VM:
```
vagrant destroy
```

# Side notes

The roles will be downloaded automatically and be put inside a `roles` folder.
